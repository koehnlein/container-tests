ARG DDEV_BASE_VERSION=latest
ARG COMPOSER_VERSION=2

FROM composer:${COMPOSER_VERSION} AS composer

FROM ddev/ddev-php-base:${DDEV_BASE_VERSION}

ARG PHP_VERSION=7.4
ARG SURF_VERSION=2

# disable xdebug by default
ENV XDEBUG_MODE=off

# remove unused repository because of key issues
# https://github.com/oerdnj/deb.sury.org/issues/2074
RUN rm /etc/apt/sources.list.d/php.list /etc/apt/sources.list.d/nginx.list

# install additional software
RUN apt update && \
    apt install -y rsync unzip patch openssh-client parallel && \
    apt clean

# install "n" to be able to add specific node version
RUN which n || npm install -g n

# pre-install current node LTS versions
# https://github.com/nodejs/release#release-schedule
RUN n 18 && n 20 && n 22

# install composer
ENV COMPOSER_ALLOW_SUPERUSER=1
ENV COMPOSER_HOME=/tmp

# copy composer from official composer image
COPY --from=composer /usr/bin/composer /usr/local/bin/composer
ENV PATH="/tmp/vendor/bin:$PATH"

# enable correct PHP version
RUN update-alternatives --set php /usr/bin/php${PHP_VERSION}

# enable xdebug
RUN phpenmod xdebug

# copy preconfigured files
COPY config/composer.json /tmp/composer.json

# Install global composer packages
RUN composer global require \
      typo3/surf:^${SURF_VERSION} \
      ergebnis/composer-normalize \
      squizlabs/php_codesniffer \
    && composer clear-cache
