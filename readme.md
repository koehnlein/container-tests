# Docker images for GitLab CI pipelines

This GitLab project and its container repository contain some docker images, based on ddev's PHP images, but with
different combinations of PHP version and Composer version. Also a global installation of `typo3/surf:^2`
is included.

## Pick your correct image

Go to container registry and copy the full identifier of the image, that matches your project software versions.

![Container registry in navigation](Documentation/navigation-registry.png)

![List of available images with marked clipboard icons](Documentation/container-list.png)

## Node versions

The containers have installed `nvm` with deeper shell integration. So all you have to is, it to add a valid `.nvmrc`
file to your project.

More information: <https://github.com/nvm-sh/nvm#nvmrc>
